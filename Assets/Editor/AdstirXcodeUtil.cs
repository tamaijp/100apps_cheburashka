using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
using System.IO;
using System.Collections.Generic;

public class AdstirXcodeUtil : MonoBehaviour
{
	internal static void CopyAndReplaceDirectory(string srcPath, string dstPath)
	{
		if (Directory.Exists(dstPath))
			Directory.Delete(dstPath);
		if (File.Exists(dstPath))
			File.Delete(dstPath);
		
		Directory.CreateDirectory(dstPath);
		
		foreach (var file in Directory.GetFiles(srcPath))
			File.Copy(file, Path.Combine(dstPath, Path.GetFileName(file)));
		
		foreach (var dir in Directory.GetDirectories(srcPath))
			CopyAndReplaceDirectory(dir, Path.Combine(dstPath, Path.GetFileName(dir)));
	}

	[PostProcessBuild]
	public static void OnPostProcessBuild(BuildTarget buildTarget, string path)
	{
		if (buildTarget == BuildTarget.iOS) {
			string projPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";
			PBXProject proj = new PBXProject();

			proj.ReadFromString(File.ReadAllText(projPath));
			string target = proj.TargetGuidByName("Unity-iPhone");

			foreach(ProjectItem item in LoadItemList("SystemFramework.tsv")) {
				proj.AddFrameworkToProject(target, item.Name, item.IsWeak);
			}
			
			foreach(ProjectItem item in LoadItemList("DynamicLibrary.tsv")) {
				proj.AddDynamicLibraryToProject(target, item.Name, item.IsWeak);
			}
			
			foreach(ProjectItem item in LoadItemList("UserFramework.tsv")) {
				string targetPath = "Frameworks/" + item.Name;
				CopyAndReplaceDirectory("Assets/Plugins/iOS/" + item.Name, Path.Combine(path, targetPath));
				proj.AddFileToBuild(target, proj.AddFile(targetPath, targetPath, PBXSourceTree.Source));
			}
			
			foreach(ProjectItem item in LoadItemList("UserResource.tsv")) {
				CopyAndReplaceDirectory("Assets/Plugins/iOS/" + item.Name, Path.Combine(path, item.Name));
				proj.AddFileToBuild(target, proj.AddFile(item.Name, item.Name, PBXSourceTree.Source));
			}
			
			proj.SetBuildProperty(target, "FRAMEWORK_SEARCH_PATHS", "$(inherited)");
			proj.AddBuildProperty(target, "FRAMEWORK_SEARCH_PATHS", "$(PROJECT_DIR)/Frameworks");

			proj.AddBuildProperty(target, "OTHER_LDFLAGS", "-ObjC");

			File.WriteAllText(projPath, proj.WriteToString());
		}
	}

	internal struct ProjectItem
	{
		public ProjectItem(string name, bool weak) {
			this.Name = name;
			this.IsWeak = weak;
		}

		public string Name { get; set; }
		public bool IsWeak { get; set; }
	}

	internal static List<ProjectItem> LoadItemList(string filename, string basedir = "Assets/Plugins/iOS")
	{
		List<ProjectItem> items = new List<ProjectItem> ();

		string[] lines = File.ReadAllLines(Path.Combine (basedir, filename));
		foreach (string line in lines) {
			string[] parts = line.Split('\t');

			if (parts.Length == 0) {
				continue;
			}

			if (parts[0].Length == 0) {
				continue;
			}

			bool weak = false;
			if (parts.Length > 1) {
				bool.TryParse(parts[1], out weak);
			}

			items.Add(new ProjectItem(parts[0], weak));
		}

		return items;
	}

}