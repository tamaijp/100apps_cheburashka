﻿using UnityEngine;
using System.Collections;

public class ShowRanking : MonoBehaviour {

    [SerializeField]
    private GameObject RankingCanvas;


    public void Show()
    {
        RankingCanvas.SetActive(true);
    }

}
