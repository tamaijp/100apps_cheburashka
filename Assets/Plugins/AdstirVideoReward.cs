using UnityEngine;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

public class AdstirVideoReward : MonoBehaviour
{
	public delegate void AdstirVideoRewardDelegate (int spot);

	public static AdstirVideoRewardDelegate OnLoad;
	public static AdstirVideoRewardDelegate OnFailToLoad;
	public static AdstirVideoRewardDelegate OnStart;
	public static AdstirVideoRewardDelegate OnComplete;
	public static AdstirVideoRewardDelegate OnCancel;
	public static AdstirVideoRewardDelegate OnClose;

	#if UNITY_IPHONE && !UNITY_EDITOR
	[DllImport("__Internal")]
	private static extern void _AdstirVideoReward_setMediaUserID(string mediaUserID);
	[DllImport("__Internal")]
	private static extern void _AdstirVideoReward_init(string media, int[] spots, int numberOfSpots);
	[DllImport("__Internal")]
	private static extern void _AdstirVideoReward_load(int spot);
	[DllImport("__Internal")]
	private static extern void _AdstirVideoReward_show(int spot);
	#elif UNITY_ANDROID && !UNITY_EDITOR
	private static AndroidJavaObject adstirPlugin;
	#endif

	private static AdstirVideoReward instance;
	private static bool initialized = false;

	private static void EnsureInstance ()
	{
		if (instance == null) {
			instance = FindObjectOfType (typeof(AdstirVideoReward)) as AdstirVideoReward;
			if (instance == null) {
				instance = new GameObject ("AdstirVideoReward").AddComponent<AdstirVideoReward> ();
			}
		}
	}

	private static string mediaUserID;

	public static void SetMediaUserID (string mediaUserID)
	{
		if (!initialized && mediaUserID != null) {
			AdstirVideoReward.mediaUserID = mediaUserID;

			#if UNITY_IPHONE && !UNITY_EDITOR
			_AdstirVideoReward_setMediaUserID(mediaUserID);
			#elif UNITY_ANDROID && !UNITY_EDITOR
			if (adstirPlugin == null) {
				adstirPlugin = new AndroidJavaObject("com.adstir.unity.AdstirPlugin");
			}
			adstirPlugin.Call("_AdstirVideoReward_setMediaUserID", mediaUserID);
			#endif
		}
	}

	public static string GetMediaUserID ()
	{
		return AdstirVideoReward.mediaUserID;
	}

	public static void Init (string media, int[] spots)
	{
		if (!initialized) {
			#if UNITY_IPHONE && !UNITY_EDITOR
			_AdstirVideoReward_init(media, spots, spots.Length);
			#elif UNITY_ANDROID && !UNITY_EDITOR
			AndroidInit(media, spots);
			#endif
			EnsureInstance ();

			initialized = true;
		}
	}

	private static void AndroidInit (string media, int[] spots)
	{
		#if UNITY_ANDROID && !UNITY_EDITOR
		if (adstirPlugin == null) {
			adstirPlugin = new AndroidJavaObject("com.adstir.unity.AdstirPlugin");
		}

		var j_media = AndroidJNI.NewStringUTF(media);
		var j_spots = AndroidJNIHelper.ConvertToJNIArray(spots);
		
		var initMethod = AndroidJNI.GetMethodID(adstirPlugin.GetRawClass(), "_AdstirVideoReward_init", "(Ljava/lang/String;[I)V");

		var args = new jvalue[3];
		args[0].l = j_media;
		args[1].l = j_spots;

		AndroidJNI.CallVoidMethod(adstirPlugin.GetRawObject(), initMethod, args);
		#endif
	}

	public static void AndroidPause()
	{
		#if UNITY_ANDROID && !UNITY_EDITOR
		adstirPlugin.Call("_AdstirVideoReward_pause");
		#endif
	}
	
	public static void AndroidResume()
	{
		#if UNITY_ANDROID && !UNITY_EDITOR
		adstirPlugin.Call("_AdstirVideoReward_resume");
		#endif
	}
	
	public static void Load (int spot)
	{
		if (initialized) {
			#if UNITY_IPHONE && !UNITY_EDITOR
			_AdstirVideoReward_load(spot);
			#elif UNITY_ANDROID && !UNITY_EDITOR
			adstirPlugin.Call("_AdstirVideoReward_load", spot);
			#endif
		}
	}

	public static void Show (int spot)
	{
		if (initialized) {
			#if UNITY_IPHONE && !UNITY_EDITOR
			_AdstirVideoReward_show(spot);
			#elif UNITY_ANDROID && !UNITY_EDITOR
			adstirPlugin.Call("_AdstirVideoReward_show", spot);
			#endif
		}
	}

	void Awake ()
	{
		name = "AdstirVideoReward";
		DontDestroyOnLoad (transform.gameObject);
	}

	public void Internal_OnLoad (String ltsv)
	{
		Debug.Log ("Internal_OnLoad");
		Dictionary<string, string> args = AdStir.LTSV.toDictionary (ltsv);
		if (null != OnLoad) {
			OnLoad (int.Parse (args ["spot"]));
		}
	}

	public void Internal_OnFailToLoad (String ltsv)
	{
		Debug.Log ("Internal_OnFailToLoad");
		Dictionary<string, string> args = AdStir.LTSV.toDictionary (ltsv);
		if (null != OnFailToLoad) {
			OnFailToLoad (int.Parse (args ["spot"]));
		}
	}

	public void Internal_OnStart (String ltsv)
	{
		Debug.Log ("Internal_OnStart");
		Dictionary<string, string> args = AdStir.LTSV.toDictionary (ltsv);
		if (null != OnStart) {
			OnStart (int.Parse (args ["spot"]));
		}
	}

	public void Internal_OnComplete (String ltsv)
	{
		Debug.Log ("Internal_OnComplete");
		Dictionary<string, string> args = AdStir.LTSV.toDictionary (ltsv);
		if (null != OnComplete) {
			OnComplete (int.Parse (args ["spot"]));
		}
	}

	public void Internal_OnCancel (String ltsv)
	{
		Debug.Log ("Internal_OnCancel");
		Dictionary<string, string> args = AdStir.LTSV.toDictionary (ltsv);
		if (null != OnCancel) {
			OnCancel (int.Parse (args ["spot"]));
		}
	}

	public void Internal_OnClose (String ltsv)
	{
		Debug.Log ("Internal_OnClose");
		Dictionary<string, string> args = AdStir.LTSV.toDictionary (ltsv);
		if (null != OnClose) {
			OnClose (int.Parse (args ["spot"]));
		}
	}
}